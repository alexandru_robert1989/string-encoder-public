package com.example.stringencoder.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

@Log4j2
@Service
public class StringEncoder {

    /**
     * The secret used for encoding and decoding String values.
     */
    @Value("${encoder.secret}")
    private String secret;

    /**
     * {@link javax.crypto.SecretKey} instance that can be used with the AES
     * encrypting algorithm.
     */
    private SecretKeySpec secretKey;


    public StringEncoder(){

    }


    /**
     * Method setting an AES encryption algorithm using a secret String value.
     *
     * @param secret the secret String value
     */
    private void setKey(String secret) {
        MessageDigest sha;
        try {
            byte[] key = secret.getBytes(StandardCharsets.UTF_8);
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method returning the encrypted value of a String.
     *
     * @param strToEncrypt the String value that will be encrypted.
     * @return the encrypted value of a String
     * @throws NoSuchPaddingException    This exception is thrown when a particular padding mechanism is requested but is not available in the environment.
     * @throws NoSuchAlgorithmException  This exception is thrown when a particular cryptographic algorithm is requested but is not available in the environment.
     * @throws InvalidKeyException       This is the exception for invalid Keys (invalid encoding, wrong length, uninitialized, etc).
     * @throws IllegalBlockSizeException This exception is thrown when the length of data provided to a block cipher is incorrect, i.e., does not match the block size of the cipher.
     * @throws BadPaddingException       This exception is thrown when a particular padding mechanism is expected for the input data but the data is not padded properly.
     */
    public String encrypt(String strToEncrypt) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {
        setKey(secret);
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        return Base64.getUrlEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));

    }


    /**
     * Method returning the decrypted value of a priory encoded String.
     *
     * @param strToDecrypt the String that will be decrypted.
     * @return a decrypted String value
     * @throws NoSuchPaddingException    This exception is thrown when a particular padding mechanism is requested but is not available in the environment.
     * @throws NoSuchAlgorithmException  This exception is thrown when a particular cryptographic algorithm is requested but is not available in the environment.
     * @throws InvalidKeyException       This is the exception for invalid Keys (invalid encoding, wrong length, uninitialized, etc).
     * @throws IllegalBlockSizeException This exception is thrown when the length of data provided to a block cipher is incorrect, i.e., does not match the block size of the cipher.
     * @throws BadPaddingException       This exception is thrown when a particular padding mechanism is expected for the input data but the data is not padded properly.
     */
    public String decrypt(String strToDecrypt) throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException,
            IllegalBlockSizeException, BadPaddingException {

        setKey(secret);
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        return new String(cipher.doFinal(Base64.getUrlDecoder().decode(strToDecrypt)));

    }




    @PostConstruct
    private  void encodingAndDecodingExample() {

        // change value for testing
        String testString = "I will be encrypted and then decrypted";
        try {
            System.out.println("\tBefore encryption :\' " + testString + "\'");
            testString = encrypt(testString);
            System.out.println("\tAfter encryption :\' " + testString + "\'");
            testString = decrypt(testString);
            System.out.println("\tAfter decryption :\' " + testString + "\'");


        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }

    }


}

# Simple String encoder and decoder example project

This project contains a simple service allowing the encoding and the 
decoding of String values.

The encoding and decoding is realised by the com.example.stringencoder.service.StringEncoder
class. 

The encodingAndDecodingExample() method, launched on startup thanks to is 
@PostConstruct annotation, prints in the console encoding and decoding examples.


## Settings

The application.properties file contains the secret used for encoding and later decoding 
String values.

Note that the application will run by default on port 8081. Comment the port value
of the properties file to run the application on default port 8080.
